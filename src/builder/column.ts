/** Dependencies */
import {
    Collection,
    Forms,
    affects,
    definition,
    editor,
    isString,
    name,
    pgettext,
} from "tripetto";
import { Matrix } from "./";

export class Column extends Collection.Item<Matrix> {
    @name
    @definition
    label = "";

    @definition
    @affects("#refresh")
    value?: string;

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:matrix", "Column"),
            form: {
                title: pgettext("block:matrix", "Column label"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "label", "")
                    )
                        .autoFocus()
                        .autoSelect(),
                ],
            },
            locked: true,
        });

        this.editor.option({
            name: pgettext("block:matrix", "Identifier"),
            form: {
                title: pgettext("block:matrix", "Column identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:matrix",
                            "If a column identifier is set, this identifier will be used instead of the label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
        });
    }
}
