/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    NodeBlock,
    affects,
    conditions,
    definition,
    editor,
    npgettext,
    pgettext,
    tripetto,
} from "tripetto";
import { Row } from "./row";
import { Column } from "./column";
import { MatrixCondition } from "./condition";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:matrix", "Matrix");
    },
})
export class Matrix extends NodeBlock {
    @definition
    columns = Collection.of(Column, this as Matrix);

    @definition
    @affects("#name")
    rows = Collection.of(Row, this as Matrix);

    @definition
    @affects("#required")
    required?: boolean;

    @definition
    @affects("#slots")
    @affects("#collection", "rows")
    exportable?: boolean;

    get label() {
        return npgettext(
            "block:matrix",
            "%2 (%1 row)",
            "%2 (%1 rows)",
            this.rows.count,
            this.type.label
        );
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.explanation();

        this.editor.collection({
            collection: this.rows,
            title: pgettext("block:matrix", "Rows"),
            placeholder: pgettext("block:matrix", "Unnamed row"),
            editable: true,
            sorting: "manual",
        });

        this.editor.collection({
            collection: this.columns,
            title: pgettext("block:matrix", "Columns"),
            placeholder: pgettext("block:matrix", "Unnamed column"),
            editable: true,
            sorting: "manual",
        });

        this.editor.groups.options();
        this.editor.required(
            this,
            pgettext("block:matrix", "All rows need an answer"),
            (required) =>
                this.rows.each((row) => (row.defineSlot().required = required))
        );
        this.editor.visibility();
        this.editor.exportable(this);
    }

    @conditions
    defineConditions(): void {
        this.rows.each((row: Row) => {
            if (row.name) {
                const group = this.conditions.group(row.name);

                this.columns.each((column: Column) => {
                    if (column.label) {
                        group.template({
                            condition: MatrixCondition,
                            label: column.label,
                            props: {
                                slot: this.slots.select(row.id),
                                row: row,
                                column: column,
                            },
                        });
                    }
                });
            }
        });
    }
}
