/** Dependencies */
import {
    Collection,
    Forms,
    Slots,
    affects,
    created,
    definition,
    deleted,
    editor,
    isBoolean,
    isString,
    name,
    pgettext,
    refreshed,
    renamed,
    reordered,
    slotInsertAction,
} from "tripetto";
import { Matrix } from "./";

export class Row extends Collection.Item<Matrix> {
    @name
    @definition
    name = "";

    @definition
    @affects("#refresh")
    alias?: string;

    @definition
    explanation?: string;

    @created
    @reordered
    @renamed
    @refreshed
    defineSlot(): Slots.String {
        return this.ref.slots.dynamic({
            type: Slots.String,
            reference: this.id,
            label: pgettext("block:matrix", "Matrix row"),
            sequence: this.index,
            name: this.name,
            alias: this.alias,
            exportable: this.ref.exportable,
            pipeable: {
                group: "Row",
                label: pgettext("block:matrix", "Row"),
                template: "value",
            },
        });
    }

    @deleted
    deleteSlot(): void {
        this.ref.slots.delete(this.id, "dynamic");
    }

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:matrix", "Row"),
            form: {
                title: pgettext("block:matrix", "Row label"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .autoFocus()
                        .autoSelect(),
                ],
            },
            locked: true,
        });

        this.editor.option({
            name: pgettext("tripetto:builder", "Help text"),
            form: {
                title: pgettext("tripetto:builder", "Help text"),
                controls: [
                    new Forms.Text(
                        "multiline",
                        Forms.Text.bind(this, "explanation", undefined)
                    )
                        .placeholder(
                            pgettext(
                                "tripetto:builder",
                                "Type help text here..."
                            )
                        )
                        .action("@", slotInsertAction(this.ref.node)),
                ],
            },
            activated: isString(this.explanation),
        });

        this.editor.group(pgettext("tripetto:builder", "Options"));

        this.editor.option({
            name: pgettext("tripetto:builder", "Required"),
            form: {
                title: pgettext("tripetto:builder", "Required"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:matrix",
                            "Row is required and cannot be skipped"
                        ),
                        Forms.Checkbox.bind(
                            this.defineSlot(),
                            "required",
                            undefined,
                            true
                        )
                    ).locked(
                        (this.ref && isBoolean(this.ref.required)) || false
                    ),
                ],
            },
            activated: isBoolean(this.defineSlot().required),
            locked: this.ref && isBoolean(this.ref.required),
        });

        this.editor.option({
            name: pgettext("block:matrix", "Identifier"),
            form: {
                title: pgettext("block:matrix", "Row identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "alias", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:matrix",
                            "If a row identifier is set, this identifier will be used instead of the label."
                        )
                    ),
                ],
            },
            activated: isString(this.alias),
        });
    }
}
