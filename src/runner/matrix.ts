/** Dependencies */
import {
    NodeBlock,
    Slots,
    Value,
    assert,
    findFirst,
    validator,
} from "tripetto-runner-foundation";
import { IMatrixRow } from "./row";
import { IMatrixColumn } from "./column";

export abstract class Matrix extends NodeBlock<{
    columns: IMatrixColumn[];
    rows: IMatrixRow[];
    required?: boolean;
}> {
    /** Contains if the matrix is required. */
    readonly required =
        this.props.required ||
        findFirst(this.props.rows, (row: IMatrixRow) => this.isRowRequired(row))
            ? true
            : false;

    /** Retrieves a matrix row slot. */
    rowSlot(row: IMatrixRow): Value<string, Slots.String> {
        return assert(this.valueOf<string>(row.id)).confirm();
    }

    /** Verifies if a row is required. */
    isRowRequired(row: IMatrixRow): boolean {
        return this.rowSlot(row).slot.required || false;
    }

    /** Verifies if a column is selected. */
    isColumnSelected(row: IMatrixRow, column: IMatrixColumn): boolean {
        return this.rowSlot(row).reference === column.id;
    }

    /** Retrieves the selected column for a row. */
    getSelectedColumn(row: IMatrixRow): IMatrixColumn | undefined {
        return findFirst(this.props.columns, (column: IMatrixColumn) =>
            this.isColumnSelected(row, column)
        );
    }

    /** Sets the selected column for a row. */
    selectColumn(row: IMatrixRow, column: IMatrixColumn | undefined): void {
        this.rowSlot(row).set(
            column && (column.value || column.label),
            column && column.id
        );
    }

    @validator
    validate(): boolean {
        return findFirst(this.props.rows, (row: IMatrixRow) => {
            const rowSlot = this.rowSlot(row);

            return (this.props.required || rowSlot.slot.required) &&
                !rowSlot.reference
                ? true
                : false;
        })
            ? false
            : true;
    }
}
