export interface IMatrixColumn {
    /** Id of the column. */
    readonly id: string;

    /** Label of the column. */
    readonly label: string;

    /** Value of the column. */
    readonly value?: string;
}
